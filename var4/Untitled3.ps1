﻿$From = "Yuliia_Tretiakova@epam.com"
$To = "Yuliia_Tretiakova@epam.com"
$Cc = "j.tretyakova@student.csn.khai.edu"
$Attachment = "C:\Users\Yuliia_Tretiakova\skripts\task5\test.txt"
$Subject = "Email Subject"
$Body = "Insert body text here"
$SMTPServer = "smtp.gmail.com"
$SMTPPort = "587"
Send-MailMessage -From $From -to $To -Cc $Cc -Subject $Subject `
-Body $Body -SmtpServer $SMTPServer -port $SMTPPort -UseSsl `
-Credential (Get-Credential) -Attachments $Attachment