﻿<#

.SYNOPSIS
This is a  Powershell script loging  

.DESCRIPTION
Function this skripts:
-clear


.NOTES
Script wrote on powershell5

.LINK
.

.Role
the script is run using task scheadule
#>


function global:Write-Log {
    Param (   
        [Parameter(Mandatory = $false)] 
        [string]$Level,     
        [Parameter(Mandatory = $false)]    
        [string]$Message, 
        [Parameter(Mandatory = $false)] 
        [switch]$NoClobber, 
        [Parameter(Mandatory = $false)] 
        [Alias('LogPath')] 
        [string]$Path = '.\lLog.log'
    ) 
    Begin { 
        $VerbosePreference = 'Continue' 
    } 
    Process {   
        if ((Test-Path $Path) -AND $NoClobber) { 
            Write-Error "Log file $Path already exists, and you specified NoClobber. Either delete the file or specify a different name." 
            Return 
        } 
        elseif (!(Test-Path $Path)) { 
            Write-Verbose "Creating $Path." 
            $NewLogFile = New-Item $Path -Force -ItemType File 
        } 
        else {      
        }  
        $FormattedDate = Get-Date -Format "yyyy-MM-dd HH:mm:ss" 
        switch ($Level) { 
            'Error' { 
                Write-Warning $Message  
                $LevelText = 'ERROR:' 
            } 
            'Verbose' { 
                $Message 
                $LevelText = 'VERBOSE' 
            } 
            'Info' { 
                $Message 
                $LevelText = 'INFO' 
            } 
            'WARNING' { 
                Write-Warning $Message
                $LevelText = 'Warning' 
            }
        }    
        "$FormattedDate $LevelText $Message" | Out-File -FilePath $Path -Append 
    } 
    End { 
    } 
}