﻿<#

.SYNOPSIS
This is a  Powershell script which get and save credential

.DESCRIPTION
Function this skripts:
-get credentials
-save credentials


.NOTES
Script wrote on powershell5

.LINK
.

.Role
Users need enter credentials
#>


$pathtxt = 'passfile.txt'
Write-Log  -Level 'Verbose' -Message  'Enter CREDENTIAL'
$global:cred = Get-Credential
$cred.Username
$cred.Password
$cred.Password | ConvertFrom-SecureString | Set-Content $pathtxt