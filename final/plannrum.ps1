﻿<#

.SYNOPSIS
This is a  Powershell script set task scheduler

.DESCRIPTION
Function this skripts:
- set task scheduler;



.NOTES
Script wrote on powershell5

.LINK
.

.Role
User need only run skrip
#>


$my_main = '.\mainpr.ps1'
$fintin_min = '.\Fiftmi.ps1'
function RunSkript {
    $time = New-JobTrigger -Daily -DaysInterval 60 -At "4:45 PM"
    $runing = New-ScheduledJobOption -RunElevated
    $Action = New-ScheduledTaskAction -Execute "PowerShell.exe" -Argument $my_main
    Register-ScheduledTask -TaskName "every60days" -Action $action -Trigger $time -RunLevel Highest -User "System" 
}
function RunMessageFiftinMin {
    $time = New-JobTrigger -Daily -DaysInterval 60 -At "4:30 PM"
    $runing = New-ScheduledJobOption -RunElevated
    $Action = New-ScheduledTaskAction -Execute "PowerShell.exe" -Argument $fintin_min
    Register-ScheduledTask -TaskName "fifrinminut" -Action $action -Trigger $time -RunLevel Highest -User "System" 
 

}
RunSkript
RunMessageFiftinMin